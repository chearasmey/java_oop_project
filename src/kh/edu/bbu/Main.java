package kh.edu.bbu;

import kh.edu.bbu.models.Teacher;
import kh.edu.bbu.services.implement.TeacherImplement;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        TeacherImplement teacherService = new TeacherImplement();
        Scanner input = new Scanner(System.in);
        int choice;
        do{
            Teacher teacher = new Teacher();
            teacherService.menuOption();
            choice = input.nextInt();

            switch (choice){
                case 1:
                    System.out.println("Teacher List");
                    teacherService.getAllTeachersWithTableGenerator(teacherService.getAllTeachers());
                    break;
                case 2:
                    System.out.println("Add new teacher");
                    System.out.println("Id:");
                    teacher.setId(input.next());
                    System.out.println("Name:");
                    teacher.setName(input.next());
                    System.out.println("Gender:");
                    teacher.setGender(input.next());
                    System.out.println("Faculty:");
                    teacher.setFaculty(input.next());
                    System.out.println("University:");
                    teacher.setUniversity(input.next());
                    System.out.println("Phone:");
                    teacher.setPhoneNumber(input.nextInt());
                    System.out.println("Email:");
                    teacher.setEmail(input.next());
                    System.out.println("Address:");
                    teacher.setAddress(input.next());
                    teacherService.addTeacher(teacher);
                    System.out.println("Teacher has been added successfully!");
                    break;
                case 3:
                    System.out.println("Add new teacher");
                    System.out.println("Id:");
                    teacher.setId(input.next());
                    System.out.println("Name:");
                    teacher.setName(input.next());
                    System.out.println("Gender:");
                    teacher.setGender(input.next());
                    System.out.println("Faculty:");
                    teacher.setFaculty(input.next());
                    System.out.println("University:");
                    teacher.setUniversity(input.next());
                    System.out.println("Phone:");
                    teacher.setPhoneNumber(input.nextInt());
                    System.out.println("Email:");
                    teacher.setEmail(input.next());
                    System.out.println("Address:");
                    teacher.setAddress(input.next());
                    teacherService.updateTeacher(teacher);
                    System.out.println("Teacher has been updated successfully!");
                    break;
                case 4:
                    System.out.println("Delete teacher");
                    System.out.println("Please input teacher Id:");
                    teacher.setId(input.next());
                    teacherService.deleteTeacher(teacher.getId());
                    System.out.println("Deleted successfully!");
                    break;
                case 5:
                    System.out.println("Search teacher by name:");
                    teacher.setId(input.next());
                    teacherService.searchTeacherByName(teacher.getId());
                    break;
                case 6:
                    choice=0;
                    break;
            }

        }while(choice!=0);
    }
}
